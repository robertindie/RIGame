﻿namespace DontTapWhiteBlock
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.BeginButton = new System.Windows.Forms.Button();
            this.Move_timer = new System.Windows.Forms.Timer(this.components);
            this.Draw_timer = new System.Windows.Forms.Timer(this.components);
            this.GameOverLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BeginButton
            // 
            this.BeginButton.Location = new System.Drawing.Point(172, 209);
            this.BeginButton.Name = "BeginButton";
            this.BeginButton.Size = new System.Drawing.Size(99, 55);
            this.BeginButton.TabIndex = 0;
            this.BeginButton.Text = "  开始游戏    按P可暂停";
            this.BeginButton.UseVisualStyleBackColor = true;
            this.BeginButton.Click += new System.EventHandler(this.GameStart);
            // 
            // Move_timer
            // 
            this.Move_timer.Interval = 1;
            this.Move_timer.Tick += new System.EventHandler(this.Update);
            // 
            // Draw_timer
            // 
            this.Draw_timer.Enabled = true;
            this.Draw_timer.Interval = 1;
            this.Draw_timer.Tick += new System.EventHandler(this.DrawUpdate);
            // 
            // GameOverLabel
            // 
            this.GameOverLabel.AutoSize = true;
            this.GameOverLabel.Font = new System.Drawing.Font("SimSun", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.GameOverLabel.ForeColor = System.Drawing.Color.Red;
            this.GameOverLabel.Location = new System.Drawing.Point(91, 200);
            this.GameOverLabel.Name = "GameOverLabel";
            this.GameOverLabel.Size = new System.Drawing.Size(288, 64);
            this.GameOverLabel.TabIndex = 1;
            this.GameOverLabel.Text = "游戏结束";
            this.GameOverLabel.Visible = false;
            this.GameOverLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 511);
            this.Controls.Add(this.GameOverLabel);
            this.Controls.Add(this.BeginButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BeginButton;
        private System.Windows.Forms.Timer Move_timer;
        private System.Windows.Forms.Timer Draw_timer;
        private System.Windows.Forms.Label GameOverLabel;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DontTapWhiteBlock
{
    public class Block
    {
        public Point location
        {
            get; set;
        }
        public struct AccuratePoint
        {
            public float X;
            public float Y;
            public AccuratePoint(float X, float Y)
            {
                this.X = X;
                this.Y = Y;
            }
            public static implicit operator AccuratePoint(Point p)
            {
                return new AccuratePoint(p.X, p.Y);
            }
        }
        public AccuratePoint accurateLocation;
        public Color color
        {
            get; set;
        }
        public Block(Point location, Color color)
        {
            this.location = location;
            accurateLocation = location;
            this.color = color;
        }
        public Block()
        {
            location = new Point(0, 0);
            accurateLocation = new Point(0, 0);
            color = Color.Gray;
        }
        /// <summary>
        /// speed为每秒走过的像素数
        /// </summary>
        /// <param name="speed"></param>
        public void Move(int locationX,float speed)
        {
            accurateLocation = new AccuratePoint(locationX, accurateLocation.Y + (speed * Time.deltaTime));
            location = new Point((int)accurateLocation.X, (int)accurateLocation.Y);
        }
    }
    public class Blocks
    {
        public Block LeftBlock
        {
            get; set;
        }
        public Block MidleBlock
        {
            get; set;
        }
        public Block RightBlock
        {
            get; set;
        }
        public Blocks()
        {
            LeftBlock = new Block();
            MidleBlock = new Block();
            RightBlock = new Block();
        }
        public Blocks(Block Left,Block Midle,Block Right)
        {
            LeftBlock = Left;
            MidleBlock = Midle;
            RightBlock = Right;
        }
        public void Move(float speed)
        {
            LeftBlock.Move(Form1.LeftDeviation,speed);
            MidleBlock.Move(Form1.MidleDeviation,speed);
            RightBlock.Move(Form1.RightDeviation,speed);
        }
        public bool Check()
        {
            if (LeftBlock.color == Form1.unsafeColor && MidleBlock.color == Form1.unsafeColor && RightBlock.color == Form1.unsafeColor)
            {
                return true;
            }
            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft;

namespace DontTapWhiteBlock
{
    public partial class Form1 : Form
    {
        public static List<Blocks> blocks = new List<Blocks>();
        public int updateTime = 30;//单位:ms
        /// <summary>
        /// 安全颜色
        /// </summary>
        public static Color safeColor = Color.Black;
        /// <summary>
        /// 不安全颜色
        /// </summary>
        public static Color unsafeColor = Color.White;
        /// <summary>
        /// 底边的高度
        /// </summary>
        public static int MinY = 518;
        /// <summary>
        /// 左方块偏移
        /// </summary>
        public static int LeftDeviation = 134;
        /// <summary>
        /// 中方块偏移
        /// </summary>
        public static int MidleDeviation = 134 + 90;
        /// <summary>
        /// 右方块偏移
        /// </summary>
        public static int RightDeviation = 134 + 90 + 90;
        /// <summary>
        /// 分数
        /// </summary>
        public static long score = 0;
        /// <summary>
        /// 窗体标题
        /// </summary>
        public static string Title = "别踩白块";
        /// <summary>
        /// 速度
        /// </summary>
        public static float speed = 3000;
        public bool IsGaming = false;
        public bool IsParse = false;
        public Form1()
        {
            InitializeComponent();
            Text = Title;
            TransparencyKey = BackColor;
        }

        void Draw()
        {
            Bitmap bmp = new Bitmap(Width, Height);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(BackColor);

            SolidBrush sb = new SolidBrush(BackColor);
            Rectangle rect = new Rectangle();
            foreach(Blocks bs in blocks)
            {
                sb = new SolidBrush(bs.LeftBlock.color);
                rect = new Rectangle(bs.LeftBlock.location, new Size(80, 80));
                g.FillRectangle(sb, rect);
                sb = new SolidBrush(bs.MidleBlock.color);
                rect = new Rectangle(bs.MidleBlock.location, new Size(80, 80));
                g.FillRectangle(sb, rect);
                sb = new SolidBrush(bs.RightBlock.color);
                rect = new Rectangle(bs.RightBlock.location, new Size(80, 80));
                g.FillRectangle(sb, rect);
            }

            g.Dispose();
            CreateGraphics().DrawImage(bmp, 0, 0);
            bmp.Dispose();
        }

        void GenerateBlocks(int locationY)
        {
            #region 已通过测试
            
            Random r = new Random();
            Blocks blocks = new Blocks();
            int n = r.Next(1, 4);
            switch (n)
            {
                case 1:
                        blocks = new Blocks(
                            new Block(new Point(0, locationY), safeColor),
                            new Block(new Point(0, locationY), unsafeColor),
                            new Block(new Point(0, locationY), unsafeColor)
                          );break;
                case 2:
                    blocks = new Blocks(
                            new Block(new Point(0, locationY), unsafeColor),
                            new Block(new Point(0, locationY), safeColor),
                            new Block(new Point(0, locationY), unsafeColor)
                          ); break;
                case 3:
                    blocks = new Blocks(
                            new Block(new Point(0, locationY), unsafeColor),
                            new Block(new Point(0, locationY), unsafeColor),
                            new Block(new Point(0, locationY), safeColor)
                          ); break;

            }
            Form1.blocks.Add(blocks);
            #endregion
        }

        void DeleteBlocks()
        {
            if (blocks[0].Check())
            {
                blocks.RemoveAt(0);
                KeydownIndex--;
                CheckUpgrad();
                return;
            }
            GameOver();
        }

        void CheckUpgrad()
        {
            if (score > 50)
            {
                speed = 4000;
            }
            if (score > 100)
            {
                speed = 5000;
            }
            if (score > 200)
            {
                speed = 6000;
            }
            if (score > 400)
            {
                speed = 7000;
            }
            if (score > 800)
            {
                speed = 8000;
            }
        }

        private void GameStart(object sender, EventArgs e)
        {
            score = 0;
            speed = 3000;
            KeydownIndex = 0;
            BeginButton.Visible = false;
            GenerateBlocks(-170);
            Move_timer.Enabled = true;
            Draw_timer.Enabled = true;
            Text = Title;
            IsGaming = true;
        }

        void ReStart()
        {
            blocks = new List<Blocks>();
            GameOverLabel.Visible = false;
            GameStart(new object(),new EventArgs());
        }

        void GameOver()
        {
            if(Microsoft.VisualBasic.Interaction.GetSetting("RIGames.DontTapWhiteBlock", "GameTimes", "gametime") == "")
            {
                Microsoft.VisualBasic.Interaction.SaveSetting("RIGames.DontTapWhiteBlock", "GameTimes", "gametime", "0");
            }
            Microsoft.VisualBasic.Interaction.SaveSetting(
                "RIGames.DontTapWhiteBlock", "GameTimes", "gametime", 
                Convert.ToString(
                    (Convert.ToInt32( Microsoft.VisualBasic.Interaction. GetSetting(
                        "RIGames.DontTapWhiteBlock", "GameTimes", "gametime"))+1
                        )
                      )
                    );
            Console.WriteLine("GameOver");
            Move_timer.Enabled = false;
            Draw_timer.Enabled = false;
            IsGaming = false;
            GameOverLabel.Visible = true;
            Microsoft.VisualBasic.Interaction.SaveSetting("RIGames.DontTapWhiteBlock", "GameData", Microsoft.VisualBasic.Interaction.GetSetting("RIGames.DontTapWhiteBlock", "GameTimes", "gametime"), Convert.ToString(score));
        }

        void MoveBlock(float speed)
        {
            foreach(Blocks bs in blocks)
            {
                bs.Move(speed);
            }
        }

        void Check()
        {
            var result = from b in blocks where b.LeftBlock.location.Y < -80 select b;
            if (result.Count() == 0)
            {
                GenerateBlocks(-170);
            }

            if (KeydownIndex < 0)
            {
                GameOver();
            }
            Check:
            try
            {
                if (blocks[KeydownIndex].Check())
                {
                    KeydownIndex++;
                    score += 3;
                    goto Check;
                }
            }
            catch (Exception)
            {

            }
            if (blocks[0].LeftBlock.accurateLocation.Y > MinY)
            {
                DeleteBlocks();
            }

        }

        private void Update(object sender, EventArgs e)
        {
            float time = Environment.TickCount;
            #region Update
            if (!IsParse)
            {
                Text = Title + "   分数:" + score;
                MoveBlock(speed);
                Check();
            }
            #endregion
            Time.deltaTime = ((Environment.TickCount - time) / 1000) == 0 ? 0.001F : ((Environment.TickCount - time) / 1000);
        }

        private void DrawUpdate(object sender, EventArgs e)
        {
            Draw();
        }

        int KeydownIndex = 0;
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (IsGaming&&!IsParse)
            {
                if (e.KeyCode == Keys.NumPad1||e.KeyCode==Keys.J)
                {
                    if (blocks[KeydownIndex].LeftBlock.color == unsafeColor)
                    {
                        GameOver();
                    }
                    else
                    {
                        blocks[KeydownIndex].LeftBlock.color = unsafeColor;
                    }
                }
                if (e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.K)
                {
                    if (blocks[KeydownIndex].MidleBlock.color == unsafeColor)
                    {
                        GameOver();
                    }
                    else
                    {
                        blocks[KeydownIndex].MidleBlock.color = unsafeColor;
                    }
                }
                if (e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.L)
                {
                    if (blocks[KeydownIndex].RightBlock.color == unsafeColor)
                    {
                        GameOver();
                    }
                    else
                    {
                        blocks[KeydownIndex].RightBlock.color = unsafeColor;
                    }
                }
            }
            if (e.KeyCode == Keys.P)
            {
                IsParse = !IsParse;
            }
            if (e.KeyCode == Keys.Space) 
            {
                Form2 form2 = new Form2();
                form2.Show();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            ReStart();
        }
    }
}

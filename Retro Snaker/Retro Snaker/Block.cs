﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Retro_Snaker
{
    public class Block
    {
        public Point location
        {
            get; set;
        }
        public Color color
        {
            get; set;
        }
        public enum Move_action
        {
            Up,Left,Right,Down,Idle
        }
        public Move_action move_state
        {
            get; set;
        }
        public Block(Point location,Color color)
        {
            this.location = location;
            this.color = color;
            move_state = Move_action.Idle;
        }
        public Block()
        {

        }
    }
    class HeadBlock : Block
    {
        public List<Body> body = new List<Body>();
        public delegate void GenerateFoodDelegate();
        public event GenerateFoodDelegate GenerateFoodEvent;
        public int score
        {
            get; set;
        }
        public HeadBlock():base(new Point(150,150),Color.Black)
        {

        }
        public void Check(Block food,Form1 form1)
        {
            if (location == food.location)
            {
                score++;
                if (score == 1)
                {
                    Form1.blocks.Add(new Body(this));
                    for(int i = 0; i < Form1.BlockCount-1; i++)
                    {
                        body.Add(new Body());
                    }
                }
                else
                {
                    for (int i = 0; i < Form1.BlockCount; i++)
                    {
                        body.Add(new Body());
                    }
                }
                GenerateFoodEvent();
            }
            var blocks1 = from b in Form1.blocks where b.location == location&&b.IsFirst==false select b;
            if (blocks1.Count() != 0)
            {
                form1.GameOver();
            }
            if(location.X<0||location.X> Form1.SceneSize * 15)
            {
                form1.GameOver();
            }
            if (location.Y < 0 || location.Y > (Form1.SceneSize-1) * 15)
            {
                form1.GameOver();
            }
            form1.Text = "贪吃蛇   Score:" + (score * 5).ToString();
        }
        public void Move()
        {
            if (move_state == Block.Move_action.Up)
            {
                location = new Point(location.X, location.Y - 15);
            }
            if (move_state == Block.Move_action.Left)
            {
                location = new Point(location.X - 15, location.Y);
            }
            if (move_state == Block.Move_action.Right)
            {
                location = new Point(location.X + 15, location.Y);
            }
            if (move_state == Block.Move_action.Down)
            {
                location = new Point(location.X, location.Y + 15);
            }
        }
    }
    public class Body : Block
    {
        public Block Parent
        {
            get; set;
        }
        public Body(Block Parent):base(Parent.location,Color.Green)
        {
            this.Parent = Parent;
        }
        public Body()
        {

        }
        public bool IsFirst = true;
        public void Move()
        {
            if (IsFirst)
            {
                IsFirst = false;
                return;
            }
            location = Parent.location;
        }
    }
}

﻿namespace Retro_Snaker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Begin = new System.Windows.Forms.Button();
            this.Move_timer = new System.Windows.Forms.Timer(this.components);
            this.GameOverLavel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Begin
            // 
            this.Begin.Location = new System.Drawing.Point(255, 270);
            this.Begin.Name = "Begin";
            this.Begin.Size = new System.Drawing.Size(92, 67);
            this.Begin.TabIndex = 0;
            this.Begin.Text = "  开始游戏   按空格可重玩   按P可暂停";
            this.Begin.UseVisualStyleBackColor = true;
            this.Begin.Click += new System.EventHandler(this.BeginButton_Click);
            // 
            // Move_timer
            // 
            this.Move_timer.Interval = 70;
            this.Move_timer.Tick += new System.EventHandler(this.Update);
            // 
            // GameOverLavel
            // 
            this.GameOverLavel.AutoSize = true;
            this.GameOverLavel.Font = new System.Drawing.Font("SimSun", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.GameOverLavel.ForeColor = System.Drawing.Color.Red;
            this.GameOverLavel.Location = new System.Drawing.Point(169, 270);
            this.GameOverLavel.Name = "GameOverLavel";
            this.GameOverLavel.Size = new System.Drawing.Size(288, 64);
            this.GameOverLavel.TabIndex = 1;
            this.GameOverLavel.Text = "游戏结束";
            this.GameOverLavel.Visible = false;
            this.GameOverLavel.Click += new System.EventHandler(this.GameOverLabel_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 645);
            this.Controls.Add(this.GameOverLavel);
            this.Controls.Add(this.Begin);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "贪吃蛇";
            this.TransparencyKey = System.Drawing.Color.White;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Begin;
        private System.Windows.Forms.Timer Move_timer;
        private System.Windows.Forms.Label GameOverLavel;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Retro_Snaker
{
    public partial class Form1 : Form
    {
        //边长:43块
        public const int SceneSize = 43;
        //吃掉一个方块生成的方块数
        public const int BlockCount = 5;
        public static List<Body> blocks;
        HeadBlock headBlock;
        Block FoodBlock;
        //暂停
        public bool IsParse = false;
        public Form1()
        {
            InitializeComponent();
            TransparencyKey = BackColor;
        }
        void Draw()
        {
            Bitmap bmp = new Bitmap(Width, Height);
            Graphics g = Graphics.FromImage(bmp);
            g.Clear(BackColor);

            foreach(Block b in blocks)
            {
                SolidBrush sb = new SolidBrush(b.color);
                Rectangle r = new Rectangle(b.location.X, b.location.Y, 15, 15);
                g.FillRectangle(sb, r);
            }
            //渲染食物方块
            SolidBrush sbF = new SolidBrush(FoodBlock.color);
            Rectangle rF = new Rectangle(FoodBlock.location.X, FoodBlock.location.Y, 15, 15);
            g.FillRectangle(sbF, rF);
            //渲染头部方块
            SolidBrush sbH = new SolidBrush(headBlock.color);
            Rectangle rH = new Rectangle(headBlock.location.X, headBlock.location.Y, 15, 15);
            g.FillRectangle(sbH, rH);

            g.Dispose();
            CreateGraphics().DrawImage(bmp, 0, 0);
            bmp.Dispose();
        }

        private void BeginButton_Click(object sender, EventArgs e)
        {
            blocks = new List<Body>();
            headBlock = new HeadBlock();
            Begin.Visible = false;
            headBlock.GenerateFoodEvent += GenerateFood;
            GenerateFood();
            Draw();
            Move_timer.Enabled = true;
            GameOverLavel.Visible = false;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (!IsParse)
            {
                if (e.KeyCode == Keys.Up && headBlock.move_state != Block.Move_action.Down)
                {
                    headBlock.move_state = Block.Move_action.Up;
                }
                if (e.KeyCode == Keys.Left && headBlock.move_state != Block.Move_action.Right)
                {
                    headBlock.move_state = Block.Move_action.Left;
                }
                if (e.KeyCode == Keys.Right && headBlock.move_state != Block.Move_action.Left)
                {
                    headBlock.move_state = Block.Move_action.Right;
                }
                if (e.KeyCode == Keys.Down && headBlock.move_state != Block.Move_action.Up)
                {
                    headBlock.move_state = Block.Move_action.Down;
                }
            }
            if (e.KeyCode == Keys.Space)
            {
                BeginButton_Click(new object(), new EventArgs());
            }
            if (e.KeyCode == Keys.P)
            {
                IsParse = !IsParse;
            }
        }
        void GenerateFood()
        {
            Random r = new Random();
            int X = r.Next(0, SceneSize-1);
            int Y = r.Next(0, SceneSize - 1);
            FoodBlock = new Block(new Point(X * 15, Y * 15), Color.Blue);
            //检测
            var result = from b in blocks where b.location == FoodBlock.location select b;
            if (result.Count() != 0)
            {
                GenerateFood();
            }
        }
        private void Update(object sender, EventArgs e)
        {
            if (!IsParse)
            {
                //移动身体
                if (blocks.Count > 0)
                {
                    for (int i = blocks.Count() - 1; i >= 0; i--)
                    {
                        blocks[i].Move();
                    }
                }
                //移动头部
                headBlock.Move();
                //检测
                headBlock.Check(FoodBlock, this);
                //添加身体
                if (headBlock.body.Count != 0)
                {
                    headBlock.body[0] = new Body(blocks[blocks.Count - 1]);
                    blocks.Add(headBlock.body[0]);
                    headBlock.body.RemoveAt(0);
                }
                //渲染
                Draw();
            }
        }
        public void GameOver()
        {
            GameOverLavel.Visible = true;
            Move_timer.Enabled = false;
        }
        private void GameOverLabel_Click(object sender, EventArgs e)
        {
            BeginButton_Click(new object(),new EventArgs());
        }
    }
}
